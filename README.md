## Smarty Renderer

This is a renderer for rendering Smarty templates into a PSR-7 Response object. It works well with Slim Framework 4.


## Installation

Install with [Composer](http://getcomposer.org):

    composer require ileafctg/smarty-slim-view

## Usage with Slim 4

```php
use IleafCtg\SmartySlimView\SmartyView;

include "vendor/autoload.php";

$app = Slim\AppFactory::create();

$app->get('/hello/{name}', function ($request, $response, $args) {
    $smartyView = new SmartyView("path/to/templates", "/path/to/templates/compile/dir");
    $data = [
        'name' => 'John'
    ];
    return $smartyView->render($response, "some-template.tpl", $data);
});

$app->run();
```

Note that you could place the SmartyView instantiation within your DI Container.  Something like this:
```
    ...
    
    // Smarty View handler
    SmartyView::class => function (ContainerInterface $container) {
        $smartySettings = $container->get('settings')['smarty'];
        $smartyView = new SmartyView(
            $smartySettings['template_dir'],
            $smartySettings['compile_dir'],
            $smartySettings['cache_dir'],
            $smartySettings['config_dir']
        );
        return $smartyView;
    },
    
    ...
```

## Usage with any PSR-7 Project
```php
//Construct the View
$smartyView = new SmartySlimView\SmartyView("path/to/templates", "/path/to/templates/compile/dir");
$data = [
    'name' => 'John'
];
// Render the template
$response = $smartyView->render(new Response(), "some-template.tpl", $data);
```

## You can interact with the Smarty object via the public `smarty` property on the class.
This is useful if you want to set/get details off of the smarty instance itself.
```
// Example
$smartyView->smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
```

## Variable escaping is turned on by default.
This is done within the contructor via the following line:
```
$this-smarty->escape_html = true;
```
See https://www.smarty.net/docs/en/variable.escape.html.tpl for details on how this works.