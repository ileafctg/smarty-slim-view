<?php

namespace IleafCtg\SmartySlimView;

/**
 * This class is used to house any custom plugins for use in Smarty templates
 */
class SmartyExtensions
{
    /**
     * Retrieves a constant from a class for use in a Smarty template.
     *
     * @param array $params Array that should contain 'class' and 'constant' keys
     *
     * @return mixed|string
     */
    public static function getClassConstant(Array $params)
    {
        $class = $params['class'] ?? '';
        $constantName = $params['constant'] ?? '';
        
        if (class_exists($class) && defined("$class::$constantName")) {
            return constant("$class::$constantName");
        }
        
        // Return an empty string if the constant is not found
        return '';
    }
}
