<?php


namespace IleafCtg\SmartySlimView;

use Psr\Http\Message\ResponseInterface;
use Smarty;

/**
 * Smarty View
 *
 * This class is a Slim Framework view helper built to leverage the Smarty templating framework.
 *
 * @link https://www.smarty.net/
 */
class SmartyView
{
    // Make it public so consumer can access it and set other configs on it if they want to.
    public Smarty $smarty;
    
    
    
    /**
     * SmartyView constructor.
     *
     * @param string|array      $templateDir The directory within which you will store your template .tpl files.
     *                                       This can be a string representing a single directory or an array of strings
     *                                       representing multiple directories.  When more than one directory is specified,
     *                                       each will be searched in turn for a match and the first match that is found
     *                                       will be used.
     *                                       You can always add to this list later with addTemplateDir() method.
     * @param string            $compileDir  Directory into which 'compiled' template files will be created when a template is
     *                                       parsed after changes.
     * @param string|null       $cacheDir    Optional.  Directory where cached rendered templates will be stored.  Only necessary if you decide to enable
     *                                       caching (i.e. $smartyView->smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);).  Default value is ./cache,
     *                                       meaning that Smarty will look for the cache/ directory in the same directory
     *                                       as the executing php script.
     * @param string|array|null $configDir   Optional. These are the directories used to store config files used in the templates.
     *                                       Default is ./configs, meaning that Smarty will look for the configs/ directory
     *                                       in the same directory as the executing php script.
     */
    public function __construct($templateDir, string $compileDir, ?string $cacheDir = null, ?string $configDir = null)
    {
        // Instantiate our Smarty object so we can get it ready.
        $this->smarty = new Smarty();
        
        $this->smarty->setTemplateDir($templateDir);
        $this->smarty->setCompileDir($compileDir);
        if ($cacheDir) {
            $this->smarty->setCacheDir($cacheDir);
        }
        if ($configDir) {
            $this->smarty->setConfigDir($configDir);
        }
        
        // Register custom plugins
        $extensions = new SmartyExtensions();
        
        // Register the instance as a plugin
        $this->smarty->registerPlugin('function', 'getClassConstant', [$extensions, 'getClassConstant']);
        
        
        // Let's escape variables used in templates by default.
        $this->smarty->escape_html = true;
    }
    
    
    
    /**
     * @param string|array $templateDir  See constructor for description of this variable.
     */
    public function addTemplateDir($templateDir)
    {
        $this->smarty->addTemplateDir($templateDir);
    }
    
    
    /**
     * Output rendered template
     *
     * @param  ResponseInterface    $response
     * @param  string               $template Template pathname relative to templates directory
     * @param  array<string, mixed> $data Associative array of template variables
     *
     * @return ResponseInterface
     *
     * @throws \SmartyException
     *
     */
    public function render(ResponseInterface $response, string $template, array $data = []): ResponseInterface
    {
        foreach ($data as $key => $value) {
            $this->smarty->assign($key, $value);
        }
        
        $response->getBody()->write($this->smarty->fetch($template));
        
        return $response;
    }
}
