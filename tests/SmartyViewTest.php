<?php

namespace IleafCtg\SmartySlimViewTest;

use IleafCtg\SmartySlimView\SmartyView;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Headers;
use Slim\Psr7\Response;
use Slim\Psr7\Stream;
use SmartyException;

class SmartyViewTest extends TestCase 
{
    
    public const MY_TEST_CONST = "east";
    
    /**
     * Test the SmartyView constructor.
     */
    public function testConstructSmartyView()
    {
        $smartyView = new SmartyView("./templates", "", "", "");
        
        $this->assertInstanceOf(SmartyView::class, $smartyView);
        
    }
    
    
    
    /**
     * Test the addTemplateDir method of SmartyView.
     */
    public function testAddTemplateDir()
    {
        $smartyView = new SmartyView(__DIR__ . "/templates", "", "", "");
        
        $templateDirs = $smartyView->smarty->getTemplateDir();
        $this->assertEquals([__DIR__ . "/templates/"], $templateDirs);
        
        $smartyView->addTemplateDir(__DIR__ . "/templates2");
        $templateDirs = $smartyView->smarty->getTemplateDir();
        $this->assertEquals([__DIR__ . "/templates/", __DIR__ . "/templates2/"], $templateDirs);

        $smartyView->addTemplateDir([__DIR__ . "/templates3", __DIR__ . "/templates4"]);
        $templateDirs = $smartyView->smarty->getTemplateDir();
        $this->assertEquals(
            [
                __DIR__ . "/templates/",
                __DIR__ . "/templates2/",
                __DIR__ . "/templates3/",
                __DIR__ . "/templates4/",
            ], $templateDirs);
    }
    
    
    
    /**
     * Test the render method of our SmartyView class.
     * 
     * @throws SmartyException
     */
    public function testRender()
    {
        $smartyView = new SmartyView(__DIR__ . "/templates", __DIR__ . "/templates_c");
        $smartyView->smarty->assign('myClass', self::class);
        
        $headers = new Headers();
        $body = new Stream(fopen('php://temp', 'r+'));
        $response = new Response(200, $headers, $body);
        
        $renderedResponse = $smartyView->render($response, "testTemplate.tpl", ['name' => 'world']);
        
        $this->assertEquals("Hello world - east", (string) $renderedResponse->getBody());
        
    }
    
}